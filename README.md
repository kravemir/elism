# Elism

This is a custom language focused on development of **e**asy, but **li**ght and **s**afe **m**emory **m**anagement. Language is being implemented as part my Bachelors work.
 
It's still work-in-progress, and implementation is highly experimental.
